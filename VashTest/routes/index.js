﻿
/*
 * GET home page.
 */

exports.index = function(req, res){
    res.render('index', {
        title: 'Express', 
        sitename: 'VASH testing website',
        isAuthenticated: req.user && req.user != false,
        username: req.user && req.user != false ? req.user.username : 'guest'
    });
};