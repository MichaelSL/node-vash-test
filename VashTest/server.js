﻿var express = require('express');
var session = require('express-session')
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var errorhandler = require('errorhandler');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser')
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var VKontakteStrategy = require('passport-vkontakte').Strategy;

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'vash');
app.use(favicon(path.join(__dirname, 'public/favicon.ico')));

app.use(bodyParser.urlencoded({ extended: false }));    // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                             // parse application/json
app.use(session({ secret: 'keyboard cat' }));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});
passport.use(new LocalStrategy(
  function (username, password, done) {
    if (username == 'root' && password == 'root')
        return done(null, { username: username });
    else
        return done(null, false);
    }
));

passport.use(new FacebookStrategy({
    clientID: 247530275301518,
    clientSecret: 'a2a687a9d0bcab8063ab746ef381796c',
    callbackURL: "/auth/facebook/callback",     //http://guarded-lake-9226.herokuapp.com/auth/facebook/callback
    profileFields: ['picture']
  },
  function(accessToken, refreshToken, profile, done) {
      done(null, {username: profile });
  }
));

passport.use(new VKontakteStrategy({
    clientID: 4555655, // VK.com docs call it 'API ID'
    clientSecret: 'xAWBjQ0933yZtgNe0FMB',
    callbackURL: "/auth/vkontakte/callback"
},
  function (accessToken, refreshToken, profile, done) {
        done(null, { username: profile });
    }
));

app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(errorhandler());
}

app.get('/', routes.index);
app.get('/users', user.list);
app.get('/login', user.login);

app.get('/logout', function (req, res, next) {
    req.logout();
    res.redirect('/');
});

app.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login'
}));

// Redirect the user to Facebook for authentication.  When complete,
// Facebook will redirect the user back to the application at
//     /auth/facebook/callback
app.get('/auth/facebook', passport.authenticate('facebook'/*, { scope: 'publish_actions' }*/));

// Facebook will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
app.get('/auth/facebook/callback', 
  passport.authenticate('facebook', {
    successRedirect: '/',
    failureRedirect: '/login'
}));


app.get('/auth/vkontakte',
  passport.authenticate('vkontakte'));

app.get('/auth/vkontakte/callback',
  passport.authenticate('vkontakte', {
    successRedirect: '/',
    failureRedirect: '/login'
}));

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});